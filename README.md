# Docker setup for local TCP UI development

## Mac Installation

### Download Docker Mac community edition
https://store.docker.com/editions/community/docker-ce-desktop-mac

### Pull this repo

### Start Docker 
Navigate to the repo root and run the following command

```
docker-compose -f docker-nginx-compose.yml up
```

### Start Node
Change `local.childrensplace.com` section in the domainMapping.js in TCP-UI Repo to the following:

```
  // --- local development --- //
  'local.childrensplace.com': {
    apiDomain: '://local.childrensplace.com/api/',
    assetsHost: 'https://local.childrensplace.com',
    apiKeys: apiKeysTable.dev
  }
};
```

Start Node
```
npm run pre-build && NODE_TLS_REJECT_UNAUTHORIZED=0 npm run node
```

### Visit TCP Local Site

https://local.childrensplace.com/us/home


## Running in IOS device browser

Replace all instances of `<<LAN computer name in DNS>>` with you Mac computer name in the `<REPO_ROOT>/nginx/default.conf` file`

  https://support.apple.com/guide/mac-help/find-your-computers-name-and-network-address-mchlp1177/mac

Uncomment lines with `<<LAN computer name in DNS>` to correctly set the cookie for local computer 
```
        proxy_cookie_domain childrensplace.com <<LAN computer name in DNS>>;
```

Change the following sections in domainMapping.js

```
  'default': {
    apiDomain: '://<<LAN computer name in DNS>/api/',
    assetsHost: 'https://<<LAN computer name in DNS>',
    apiKeys: apiKeysTable.dev
  },

  // --- local development --- //
  'local.childrensplace.com': {
    apiDomain: '://<<LAN computer name in DNS>>/api/',
    assetsHost: 'https://<<LAN computer name in DNS>>',
    apiKeys: apiKeysTable.dev
  }
```

Restart Node and Docker

In IOS browser, connect to your node instance using the `<<LAN computer name in DNS>>`

E.g. `<<LAN computer name in DNS>> = boshreid1234.local`

```
https://boshreid1234.local
```

## Change upstream environment

Find and replace `uatlive2` with new subdomain in the `<REPO_ROOT>/nginx/default.conf` file.  E.g. `uatlive1`

Restart docker
